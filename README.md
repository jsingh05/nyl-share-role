Aero-Share-Role
=========

This role utilizes the NetApp API to create, update, and delete shares on a connected NetApp Cluster. The latest documentaion on the Ansible/NetApp API used by this role can be found at: https://docs.ansible.com/ansible/latest/collections/netapp/ontap/na_ontap_cifs_server_module.html.

Requirements
------------

This role requires:

1. Python, Version 3.6.9
2. netapp.ontap, Version 21.10.0
3. netapp-lib, Version 2021.6.25

Role Variables
--------------

*netapp_hostname:*

   - This variable should be set to the NetApp hostname that is used to manage the shares for this inventory file
   
*netapp_username:*

   - This variable should be set to the NetApp username that is used to manage the shares for this inventory file
   
*netapp_password:*

   - This variable should be set to the NetApp password that is used to manage the shares for this inventory file

*provision_type:*

   - This is the execution tag that is associated with each execution, and can be set with "create", "update", or "delete". This will default to "create"

*share_properties:*

   - This variable will list all the required properties for the managed shares

*symlink_properties:*

   - This variable will list all the required syslink properties for the managed shares

*path:*

   - This variable will set the file system path that is shared through this share. The path is the full, user visible path relative to the VServer root, and it might be crossing junction mount points. The path is in UTF8 and uses forward slash as directory separator

*svm:*

   - This variable will set the VServer that will be used for this share
   
*ready_for_deletion:*

   - This variable acts as a flag to denote if this share is ready for deletion. If set to false, then when the deletion task execution is ran this host will be skipped over


Example Playbook
----------------

    ---
    - name: Manage Share
      hosts:
        - all
      gather_facts: false
      become: false
      vars_files:
        - vars/ontap_creds.yml
        - vars/shares.yml

      roles:
        - aero-share-role


Author Information
------------------

This role was developed by Enterprise Vision Technologies (*www.evtcorp.com*)
